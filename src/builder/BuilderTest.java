package builder;

import static org.junit.Assert.*;

import org.junit.Test;

public class BuilderTest {

	@Test
	public void contruirInstanciaTest() {
		DatosNutricionales cocaCola = new DatosNutricionales.Constructor(100, 500).setCalorias(99).build();
		
		assertTrue(cocaCola.getCalorias() == 99);

		
	}

}

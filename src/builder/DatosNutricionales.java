package builder;

public class DatosNutricionales {
	private final int _tamaņoPorcion;
	private final int _cantidadDePorciones;
	private final int  _calorias;
	private final int  _grasas;
	private final int  _sodio;
	private final int  _carbohidratos;
	
	public static class Constructor implements Builder<DatosNutricionales>{
		//Parametros requeridos
		private  int _tamaņoPorcion;
		private  int _cantidadDePorciones;
		
		// parametros opcionales
		private  int  _calorias;
		private  int  _grasas;
		private  int  _sodio;
		private  int  _carbohidratos;
			
		public Constructor(int tamaņoPorcion, int cantidadDePorcion){
			_tamaņoPorcion = tamaņoPorcion;
			_cantidadDePorciones = cantidadDePorcion;
		}
		
		public Builder<DatosNutricionales> setCalorias(int calorias){_calorias = calorias; return this;}
		public Builder<DatosNutricionales> setGrasas(int grasas){ _grasas = grasas; return this;}
		public Builder<DatosNutricionales> setSodio(int sodio){ _sodio = sodio; return this;}
		public Builder<DatosNutricionales> setCarbohidratos(int carbohidratos){_carbohidratos = carbohidratos; return this;}
			
		@Override
		public DatosNutricionales build() {
			return new DatosNutricionales(this);
		}
	}
	
	private DatosNutricionales(Constructor builder){
		_tamaņoPorcion = builder._tamaņoPorcion;
		_cantidadDePorciones = builder._cantidadDePorciones;
		_calorias = builder._calorias;
		_sodio = builder._sodio;
		_grasas = builder._grasas;
		_carbohidratos = builder._carbohidratos;
	}

	public int getTamaņoDePorcion(){return _tamaņoPorcion;}
	public int getCantidadDePorcion(){return _cantidadDePorciones;}
	public int getCalorias(){return _calorias;}
	public int getGrasas(){return _grasas;}
	public int getSodio(){return _sodio;}
	public int getCarbohidratos(){return _carbohidratos;}
	
}
